import React, {useState} from 'react';
import {nanoid} from 'nanoid';
import './App.css';
import CurrentOrder from "../../components/CurrentOrder/CurrentOrder";
import CurrentFood from "../../components/CurrentFood/CurrentFood";
import TotalPrice from "../../components/TotalPrice/TotalPrice";

const App = () => {

    const [foods, setFoods] = useState([
        {id: nanoid(), name: "Hamburger", price: 80, count: 0},
        {id: nanoid(), name: "Cheeseburger", price: 90, count: 0},
        {id: nanoid(), name: "Fries", price: 45, count: 0},
        {id: nanoid(), name: "Coffee", price: 70, count: 0},
        {id: nanoid(), name: "Tea", price: 50, count: 0},
        {id: nanoid(), name: "Cola", price: 40, count: 0},
    ]);

    const [showOrder, setShowOrder] = useState(false);

    const orderFood = (id) => {
        const index = foods.findIndex(food => food.id === id);
        const foodsCopy = [...foods];
        const foodCopy = {...foodsCopy[index]};
        foodCopy.count++;
        foodsCopy[index] = foodCopy;
        setFoods(foodsCopy);
        toggleOrder();
    };

    const removeFood = (id) => {
      const index = foods.findIndex(food => food.id === id);
        const foodsCopy = [...foods];
        const foodCopy = {...foodsCopy[index]};
        foodCopy.count = 0;
        foodsCopy[index] = foodCopy;
        setFoods(foodsCopy);
        toggleOrder();
    };


    const toggleOrder = () => {
      const order = foods.reduce((acc, food) => {
          acc += food.count;
          return acc;
      }, 1);
        if (order === 0) {
            setShowOrder(false);
        } else {
            setShowOrder(true);
        }
    };

    const totalPrice = foods.reduce((acc, food) => {
        if (food.count > 0) {
            return acc + food.count*food.price;
        }
        return acc;
    },0);

    const currentOrders = (
        <div>{foods.map((food) => {
            if (food.count > 0) {
                return <CurrentOrder name={food.name}
                                  id={food.id}
                                  count={food.count}
                                  price={food.price}
                                  removes={removeFood}/>
            }
        })}
        </div>);

  return (
    <div className="App">
        <div className="container">
            <div className="orders">
                <div className="title">ORDERS:</div>
                <div>{
                    showOrder ?
                        <TotalPrice currentOrders={currentOrders}
                        totalPrice={totalPrice}/> : <div>{"Order is empty! Please add some items."}</div>
                }
                </div>
            </div>

            <div className="foods">
                <div className="title">FOODS:</div>
                {foods.map((food) => {
                    return   <CurrentFood
                        id={food.id}
                        name={food.name}
                        price={food.price}
                        count={food.count}
                        orders={orderFood}/>
                })}
            </div>
        </div>
    </div>
  );
};

export default App;
