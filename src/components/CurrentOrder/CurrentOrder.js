import React from 'react';

const CurrentOrder = (props) => {
    return (<div><div className="food-name">{props.name}</div>
                <div className="food-count">x {props.count}</div>
                <div className="food-list">{props.count*props.price} KGS</div>
                <div className="food-list"><button onClick={() => props.removes(props.id)}>X</button></div>
            </div>);
};

export default CurrentOrder;