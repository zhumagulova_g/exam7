import React from 'react';

const TotalPrice = (props) => {
    return (<div>{props.currentOrders}
                <div className="line" />
                <div><span className="total">Total price: </span>
                    <span className="total">{props.totalPrice} KGS</span></div>
            </div>
    );
};

export default TotalPrice;