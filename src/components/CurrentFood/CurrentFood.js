import React from 'react';
import foodImg from '../../img/food.png';

const CurrentFood = (props) => {
    return (<div className="order"
             key={props.id}
             onClick={() => props.orders(props.id)}
        >
            <img src={foodImg} width="80px" alt="Current food"/>
            <div>{props.name}<br/> Price: {props.price} KGS</div>
        </div>);
};

export default CurrentFood;